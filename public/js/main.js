for(var i = 0; i < 1; i++) {
  
  var url = "gyro/" + nights[i]._id;
  var id = 'chart-' + (i + 1);
  
  $.getJSON( url, function( data ) {
    console.log(id);
      Morris.Line({
        element: id,
        data: data,
        xkey: 'timestamp',
        ykeys: ['inclination'],

        //axes: 'x',
        
        grid: false,
        continuousLine: true,
        resize: true,

        lineColors: ['#fff'],
        pointStrokeColors: ['#000'],
        pointSize: 0,

        gridTextFamily: 'HelveticaNeue-Medium',
        gridTextSize: '14px',
        gridTextColor: '#fff'    
      });

  });

}

$(function() {
  var now = moment();
  var socket = new io();

  $('.details .relative').each(function (i, e) {
    var time = moment($(e).parent().attr('from'));
    var diff = now.diff(time, 'days');

    $(e).html(time.format('MMMM Do'));
  });

  $('.details .duration').each(function (i, e) {
    var from = moment($(e).parent().attr('from'));
    var to = moment($(e).parent().attr('to'));

    var duration = moment.duration(to.diff(from));
    $(e).html(duration.hours() + ":" + duration.minutes() + ":" + duration.seconds());
  });

  $('.details .times').each(function (i, e) {
    var from = moment($(e).parent().attr('from'));
    var to = moment($(e).parent().attr('to'));

    $(e).html(from.hours() + ":" + from.minutes() + " \u2013 " + to.hours() + ":" + to.minutes());
  });

  socket.connect('http://localhost:3000', {
    autoConnect: true
  });

  console.log(trigger);

  if(trigger == -1) {
    var started = true;
    $('.control').html('Stop Tracking');
  } else {
    var started = false;
  }

  $('body').click(function() {
    socket.send("Go");
  });

  $('.control').click(function() {
      started = !started;
      console.log(trigger);

      if(started) {
          $(this).html('Stop Tracking');
          socket.send(1);
      } else {
          $(this).html('Start Tracking');
          socket.send(0);
      }
  });

});

