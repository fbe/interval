var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gyroSchema = new Schema({
    nightId: Schema.ObjectId,
    timestamp: String,
    x: Number,
    y: Number,
    z: Number,
    pitch: Number,
    roll: Number,
    acceleration: Number,
    inclination: Number,
    orientation: Number,
    celsius: Number
}, {
    versionKey: false
});

mongoose.model('gyro', gyroSchema, 'gyro');