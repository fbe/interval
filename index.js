var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    cron = require('cron'),
    five = require('johnny-five'),
    io = require('socket.io'),
    fs = require('fs'),
    jade = require('jade'),
    socket = io.listen(server),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    board = new five.Board(),
    port = 3000;

// Set up express
app.use('/', express.static(__dirname + '/public'));
if (app.get('env') === 'development') {
  app.locals.pretty = true;
}

// Set up jade
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// Connect to database
mongoose.connect('mongodb://127.0.0.1:27017/nights');

// Require all mongoose models
fs.readdirSync(__dirname + '/models').forEach(function(filename) {
  if (~filename.indexOf('.js')) require(__dirname + '/models/' + filename)
});

// Declare mongoose collections
var Details = mongoose.model('details');
var Gyro = mongoose.model('gyro');

var trigger = 0;

socket.on('connection', function(client) {
    client.on('message', function(message) {
        if(message == 1) {
            createNight();
            trigger = -1;
            console.log('Night created.');
        } else if(message == 0) {
            trigger = 0;
            var nightId;
            Details.find(function(err, data) {
                nightId = data[0]._id;
                Details.findOneAndUpdate({_id: nightId}, { end: new Date().toISOString() }, {}, function(err) {
                    if(err) throw err;
                });
            }).sort([['start', 'descending']]).limit(1); 

            console.log('Night ended.');
        }
    });
});

// Accelometer
board.on('ready', function() {
    var accel = new five.IMU({
        controller: 'MPU6050'
    });

    var accelValues = [];

    var everySecond = cron.job("* * * * * *", function(){
        accelValues.push({
            x: accel.accelerometer.x,
            y: accel.accelerometer.y,
            z: accel.accelerometer.z,
            pitch: accel.accelerometer.pitch,
            roll: accel.accelerometer.roll,
            acceleration: accel.accelerometer.acceleration,
            inclination: accel.accelerometer.inclination,
            orientation: accel.accelerometer.orientation,
            celsius: accel.temperature.celsius
        });
        console.log(accelValues);
        console.log("Value pushed in object.");
    });

    var everyMinute = cron.job("0 * * * * *", function(){
        var aX = average(accelValues, "x");
        var aY = average(accelValues, "y");
        var aZ = average(accelValues, "z");
        var aPit = average(accelValues, "pitch");
        var aRoll = average(accelValues, "roll");
        var aAccel = average(accelValues, "acceleration");
        var aIncl = average(accelValues, "inclination");
        var aOr = average(accelValues, "orientation");
        var aCel = average(accelValues, "celsius");

        accelValues = [];
        saveGyro(aX, aY, aZ, aPit, aRoll, aAccel, aIncl, aOr, aCel);
    });

    socket.on('connection', function(client) {
        client.on('message', function(message) {
            if(message == 1) {
                everySecond.start();
                everyMinute.start();
            } else if(message == 0) {
                everySecond.stop();
                everyMinute.stop();
            }
        });
    });

    function saveGyro(x, y, z, pitch, roll, acceleration, inclination, orientation, celsius) {

        Details.find(function(err, data) {
            if (err) throw err;
            var nightId = data[0]._id;

            var motion = new Gyro({
                nightId: nightId,
                timestamp: new Date().toISOString(),
                x: x,
                y: y,
                z: z,
                pitch: pitch,
                roll: roll,
                acceleration: acceleration,
                inclination: inclination,
                orientation: orientation,
                celsius: celsius
            });

            motion.save(function(err) {
                if (err) throw err;
              console.log('Gyro values saved!');
            });
        }).sort([['start', 'descending']]).limit(1); 
    };
});

// Function to get latest night (the one that started)
function getLatestNight() {
    var nightId;

    Details.find(function(err, data) {
       nightId = data[0]._id;
    }).sort([['start', 'descending']]).limit(1); 

    return nightId;
};

// Function to create night
function createNight() {
    var night = new Details({
        start: new Date().toISOString(),
        end: new Date().toISOString()
    });

    night.save(function(err) {
        if (err) throw err;
    });
};

// Get average number (object, key as string [e.g. "x", "y"])
function average(obj, key) {
    var sum = 0;
    for(var i = 0; i < obj.length; i++){
        sum += obj[i][key];
    }

    var avg = sum/obj.length;
    return avg;
}

/*
       ###    ########  #### 
      ## ##   ##     ##  ##  
     ##   ##  ##     ##  ##  
    ##     ## ########   ##  
    ######### ##         ##  
    ##     ## ##         ##  
    ##     ## ##        #### 
*/

// Set up root route
app.get('/', function (req, res) {
    Details.find(function(err, data) {
        res.render('index', { nights: data, trigger: trigger });
    }).sort([['start', 'descending']]).limit(7); 
});

// Set up /details API
app.get('/details', function (req, res) {
    Details.find(function(err, details) {
        if(err) throw err;
        res.send(details);
    })
});

// Set up /gyro API
app.get('/gyro/:nightId', function (req, res) {
    Gyro.find({nightId: req.params.nightId}, function(err, gyro) {
        res.send(gyro);
    })
});

// Run
console.log('listening on port http://localhost:' + port);
server.listen(port, '0.0.0.0');